package pp2.stockeando.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class UserContactTest {

	
	@Test
	public void testToStringUserContact() {
		UserContact contact = new UserContact("juan.moyano05@gmail.com");
		assertEquals("juan.moyano05@gmail.com", contact.toString());
	}
}
