package pp2.stockeando.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.awt.event.ActionEvent;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.junit.Test;

import pp2.stockeando.controller.Controller;
import pp2.stockeando.stockSearcher.MockStockSearcher;
import pp2.stockeando.utils.PropertiesFileTest;

public class SearchModelTest {

	private SearchModel model = new SearchModel();
	
	@Test
	public void testSearchModel() {
		model.setPathNotifiersData(PropertiesFileTest.getPropertieTest("pathNotifierTest"));
		model.setPathPageService(PropertiesFileTest.getPropertieTest("pathPageListJson"));
		model.setSearcher(new MockStockSearcher());
		
		List<String> pages = model.getPages();
		List<String> channelsNotifier = model.getChannelsNotifier();
		
		assertTrue(channelsNotifier.size()>0);
		model.setAction("Buscando");
		assertEquals("Buscando", model.getAction());
		
		UserContact contactTest = new UserContact("pp2tests@gmail.com");
		assertTrue(model.isReceiverInfoValid("Email", contactTest));
		
		model.setSubscribeController(new Controller() {
			
			@Override
			public void actionPerformed(ActionEvent e) {}
			
			@Override
			public void updateView() {}
		});

		Map<String,UserContact> map = new TreeMap<>();
		map.put("Email",contactTest);
		model.searchProduct(pages, "Ryzen 7", map);
		
		assertEquals("Notificacion enviada correctamente por Email", model.getAction());
		
	}
	
	
}
