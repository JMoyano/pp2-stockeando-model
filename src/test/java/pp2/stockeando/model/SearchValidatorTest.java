package pp2.stockeando.model;

import org.junit.Test;
import pp2.stockeando.utils.TestUtils;
import pp2.stockeando.validators.SearchValidator;

public class SearchValidatorTest {


    @Test
    public void given_productName_returnTrue(){
        TestUtils.assertTrue(true,SearchValidator.isProductNameValid("Nvidia gtx 3080"));
    }

    @Test
    public void given_productName2_returnTrue(){
        TestUtils.assertTrue(true,SearchValidator.isProductNameValid("TV LG 40''"));
    }

    @Test
    public void given_invalidProductName_returnFalse(){
        TestUtils.assertTrue(false,SearchValidator.isProductNameValid(" "));
    }

    @Test
    public void given_invalidProductName2_returnFalse(){
        TestUtils.assertTrue(false,SearchValidator.isProductNameValid(null));
    }

    @Test
    public void given_invalidProductName3_returnFalse(){
        TestUtils.assertTrue(false,SearchValidator.isProductNameValid(""));
    }
}
