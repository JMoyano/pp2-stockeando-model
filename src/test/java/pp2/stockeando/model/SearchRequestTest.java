
package pp2.stockeando.model;

import org.junit.Test;
import pp2.stockeando.utils.TestUtils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SearchRequestTest {

    private SearchRequest searchRequest;

    @Test
    public void given_validSearchRequest_returnTrue(){
        Page[] pages = null;
		try {
			pages = new Page[]{new Page("Mercadolibre", new URL("http://www.mercadolibre.com.ar"))};
		} catch (MalformedURLException e) {}
        searchRequest = buildSearchRequest(pages,"Nvidia GTX 3080");
        TestUtils.assertSearchRequest(true,searchRequest);
    }

    @Test
    public void given_validSearchRequest2_returnTrue(){
        Page[] pages = null;
		try {
			pages = new Page[]{new Page("Mercadolibre", new URL("http://www.mercadolibre.com.ar")),
			        new Page("CompraGamer", new URL("http://www.compragamer.com"))};
		} catch (MalformedURLException e) {}
        searchRequest = buildSearchRequest(pages,"Ryzen 7");
        TestUtils.assertSearchRequest(true,searchRequest);
    }

    @Test
    public void given_validSearchRequest3_returnTrue(){
        Page[] pages = null;
		try {
			pages = new Page[]{new Page("Mercadolibre", new URL("http://www.mercadolibre.com.ar")),
			        new Page("CompraGamer", new URL("http://www.compragamer.com"))};
		} catch (MalformedURLException e) {}
        searchRequest = buildSearchRequest(pages,"Ryzen");
        TestUtils.assertSearchRequest(true,searchRequest);
    }


    @Test(expected = MalformedURLException.class)
    public void given_invalidSearchRequest_throwError() throws MalformedURLException{
        Page[] pages = new Page[]{new Page("Mercadolibre", new URL("https"))};
        searchRequest = buildSearchRequest(pages,"Nvidia GTX 3080");
    }

    @Test(expected = IllegalArgumentException.class)
    public void given_invalidSearchRequest2_throwError(){
        Page[] pages = null;
		try {
			pages = new Page[]{new Page(" ", new URL("http://www.mercadolibre.com.ar"))};
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        searchRequest = buildSearchRequest(pages,"Nvidia GTX 3080");
    }


    @Test(expected = IllegalArgumentException.class)
    public void given_invalidSearchRequest5_throwError(){
        Page[] pages = null;
		try {
			pages = new Page[]{new Page("Mercadolibre", new URL("http://www.mercadolibre.com.ar"))};
		} catch (MalformedURLException e) {}
        searchRequest = buildSearchRequest(pages," ");
    }

    @Test(expected = IllegalArgumentException.class)
    public void given_invalidSearchRequest6_throwError(){
        Page[] pages = new Page[]{};
        searchRequest = buildSearchRequest(pages,"Nvidia GTX 3080");
    }



   


    private SearchRequest buildSearchRequest(Page[] pages, String productName){
        List<Page> pageList = new ArrayList<>(
                Arrays.asList(pages));

        return new SearchRequest(pageList,productName);
    }
}
