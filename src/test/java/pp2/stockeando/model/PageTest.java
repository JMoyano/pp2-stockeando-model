package pp2.stockeando.model;

import java.net.MalformedURLException;
import java.net.URL;

import org.junit.Test;
import pp2.stockeando.utils.TestUtils;

public class PageTest {

    private Page page;

    @Test
    public void given_validPage_returnTrue(){

        try {
			page = new Page("Mercadolibre",new URL("http://www.mercadolibre.com.ar/"));
			TestUtils.assertTrue("Mercadolibre",page.getPageName());
	        TestUtils.assertTrue(new URL("http://www.mercadolibre.com.ar/"),page.getUrl());
		} catch (MalformedURLException e) {}
        
    }

    @Test
    public void given_validPage2_returnTrue(){
    	 try {
	        page = new Page("CompraGamer",new URL("http://www.compragamer.com.ar/"));
	        TestUtils.assertTrue("CompraGamer",page.getPageName());
	        TestUtils.assertTrue(new URL("http://www.compragamer.com.ar/"),page.getUrl());
    	 } catch (MalformedURLException e) {}
    }


    @Test(expected = MalformedURLException.class)
    public void given_invalidUrl2_throwError() throws MalformedURLException{
        page = new Page("CompraGamer",new URL("compragamer.com.ar/"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void given_invalidName_returnfalse(){
        try {
			page = new Page("",new URL("http://compragamer.com.ar/"));
		} catch (MalformedURLException e) {}
    }

    @Test(expected = IllegalArgumentException.class)
    public void given_invalidName2_returnfalse(){
        try {
			page = new Page(" ",new URL("http://compragamer.com.ar/"));
		} catch (MalformedURLException e) {}
    }

    @Test(expected = IllegalArgumentException.class)
    public void given_invalidName3_returnfalse(){
        try {
			page = new Page(null,new URL("http://compragamer.com.ar/"));
		} catch (MalformedURLException e) {}
    }
}
