package pp2.stockeando.stockSearcher;

import static org.junit.Assert.assertTrue;

import java.net.MalformedURLException;
import java.net.URL;

import org.junit.Test;

import pp2.stockeando.utils.PropertiesFileTest;

public class StockSearcherJsonTest {

	@Test
	public void testSearcher() {
		StockSearcherJson ss = new StockSearcherJson();
		ss.setPath(PropertiesFileTest.getPropertieTest("pathStockInPagesJson"));
		
		try {
			assertTrue(ss.search("Ryzen 7", new URL("http://www.compragamer.com")));
		} catch (MalformedURLException e) {}
	}
}
