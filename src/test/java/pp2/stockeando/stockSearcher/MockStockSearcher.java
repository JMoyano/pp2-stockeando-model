package pp2.stockeando.stockSearcher;

import java.net.MalformedURLException;
import java.net.URL;

public class MockStockSearcher implements StockSearcher {

	@Override
	public boolean search(String nameProduct, URL url) {
	     try {
	    	URL ML = new URL("http://www.mercadolibre.com.ar");
			URL CG = new URL("http://www.compragamer.com");
			
			if(url.equals(ML)) {
				switch(nameProduct) {
					case "Ryzen 7":
						return true;
					case "Nvidia Rtx 3080":
						return true;
					case "RAM DDR4 8Gb":
						return true;
					default:
						return false;
				}
			}
			else if(url.equals(CG)) {
				switch(nameProduct) {
					case "Ryzen 7":
						return true;
					case "Nvidia Rtx 3080":
						return true;
					case "RAM DDR4 8Gb":
						return true;
					case "Intel Core i7":
						return true;
					default:
						return false;
				}
			}else {
				return false;
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	    
	     return false;
	}
}
