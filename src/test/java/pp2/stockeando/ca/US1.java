package pp2.stockeando.ca;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.net.MalformedURLException;
import java.net.URL;

import org.junit.Test;

import pp2.stockeando.stockSearcher.MockStockSearcher;
import pp2.stockeando.stockSearcher.StockSearcher;

public class US1 {

	private StockSearcher stockSearcher = new MockStockSearcher();
	
	@Test
	public void CA1() {
		String productName = "Intel Core i7";
		URL url;
		try {
			url = new URL("http://www.compragamer.com");
			assertTrue(stockSearcher.search(productName, url));
		} catch (MalformedURLException e) {}
		
	}
	
	@Test
	public void CA2() {
		String productName = "Intel Core i7";
		URL url;
		try {
			url = new URL("http://www.mercadolibre.com.ar");
			assertFalse(stockSearcher.search(productName, url));
		} catch (MalformedURLException e) {}
		
	}
}
