package pp2.stockeando.ca;

import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Set;

import org.junit.Test;

import pp2.stockeando.model.UserContact;
import pp2.stockeando.notifier.Notifier;
import pp2.stockeando.notifier.NotifierData;
import pp2.stockeando.utils.PropertiesFileTest;
import pp2.stockeando.utils.TestUtils;

public class US2 {
	
	private NotifierData notifierData;
	
	@Test
    public void CA1(){
	   notifierData = new NotifierData(PropertiesFileTest.getPropertieTest("pathNotifierTest"));
       Set<Notifier> notifierSet = notifierData.getNotifiers();
       TestUtils.assertTrue(2,notifierSet.size());
    }
	
	@Test
    public void CA2(){
       notifierData = new NotifierData(PropertiesFileTest.getPropertieTest("pathNotNotifierTest"));
       Set<Notifier> notifierSet = notifierData.getNotifiers();
       TestUtils.assertTrue(0,notifierSet.size());
    }
	
	@Test
	public void CA3() {
		 notifierData = new NotifierData(PropertiesFileTest.getPropertieTest("pathNotifierTest"));
		 List<String> notifiersAvailables = notifierData.getNamesNotifiersAvailable();
		 
		 assertTrue(notifiersAvailables.contains("Email"));
		 
		 UserContact conctactTest = new UserContact("pp2tests@gmail.com");
		 assertTrue(notifierData.validInfoReceiverByNameNotifier("Email", conctactTest));
		
		 Notifier notifier = notifierData.getNotifierByName("Email");
		 assertTrue(notifier.send("MensajePrueba", conctactTest));
	}
}
