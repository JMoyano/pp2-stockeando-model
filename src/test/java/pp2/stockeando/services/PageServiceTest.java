package pp2.stockeando.services;

import org.junit.Test;
import pp2.stockeando.model.Page;
import pp2.stockeando.services.impl.PageServiceJson;
import pp2.stockeando.utils.TestUtils;

import static org.junit.Assert.assertEquals;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;


public class PageServiceTest {

    private PageService pageService = new PageServiceJson("src/test/resources");

    @Test
    public void givenService_findAll_returnsList() throws MalformedURLException{

        List<Page> pageList = pageService.getAllPages();

        TestUtils.assertTrue(true,pageList.size()==2);
        TestUtils.assertTrue(true,pageList.get(0).equals(new Page("Mercadolibre",new URL("http://www.mercadolibre.com.ar"))));
        TestUtils.assertTrue(true,pageList.get(1).equals(new Page("CompraGamer",new URL("http://www.compragamer.com"))));
    }
    
    @Test
    public void givenService_findAll_getNames_findByNames() throws MalformedURLException{

        List<Page> pageList = pageService.getAllPages();

        List<String> pageNamesList = pageService.getAllNamePages();
        List<Page> pageListWithNames = pageService.getListPagesWithName(pageNamesList);
        
        assertEquals(pageList,pageListWithNames);
        
    }
}
