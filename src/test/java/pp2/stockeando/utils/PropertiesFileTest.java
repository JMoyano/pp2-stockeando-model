package pp2.stockeando.utils;

import java.io.InputStream;
import java.util.Properties;

public class PropertiesFileTest {
	 
	public static String getPropertieTest(String propertieName) {
		 String resourceName = "configTest.properties"; 
		 ClassLoader loader = Thread.currentThread().getContextClassLoader();
		 Properties props = new Properties();
		 InputStream resourceStream = loader.getResourceAsStream(resourceName);
		 try {
			 props.load(resourceStream); 
			 return  props.getProperty(propertieName);
		 }catch (Exception e) {
			 e.printStackTrace();
		 }
		 return "";
	 }
}
