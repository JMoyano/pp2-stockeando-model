package pp2.stockeando.utils;

import org.junit.Test;
import pp2.stockeando.model.Page;
import pp2.stockeando.stockSearcher.SearchStockResult;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.TreeMap;

public class MessageConstructorTest {

    private SearchStockResult processResult;
    private String productName;

    @Test
    public void given_Results_constructMessage1(){
        processResult = constructProcessResult(new String[]{"page1"},true);
        productName = "Ryzen 7";
        String message = MessageConstructor.constructMessage(processResult,productName);

        TestUtils.assertTrue(true,message.equals("Se encontro el producto Ryzen 7 en:\n"
        + "page1, http://www.page1.com\n"
        + "Gracias por usar Stockeando!"));
    }

    @Test
    public void given_Results_constructMessage2(){
        processResult = constructProcessResult(new String[]{"page1","page2"},true);
        productName = "Nvidia Rtx 3080";
        String message = MessageConstructor.constructMessage(processResult,productName);

        TestUtils.assertTrue(true,message.equals("Se encontro el producto Nvidia Rtx 3080 en:\n"
                + "page1, http://www.page1.com\n"
                + "page2, http://www.page2.com\n"
                + "Gracias por usar Stockeando!"));
    }

    @Test
    public void given_Results_constructMessage3(){
        processResult = constructProcessResult(new String[]{"page3"},false);
        productName = "RAM DDR4 8Gb";
        String message = MessageConstructor.constructMessage(processResult,productName);
        TestUtils.assertTrue(true,message.equals("No se encontro el producto RAM DDR4 8Gb en ninguna de las paginas seleccionadas.\n"
                + "Gracias por usar Stockeando!"));
    }



    private SearchStockResult constructProcessResult(String[]pages,Boolean allFlag){
        return new SearchStockResult(constructResults(pages,allFlag));
    }

    private Map<Page,Boolean> constructResults(String[] pages,Boolean allFlag){
        Map<Page,Boolean> results = new TreeMap<>();
        for (int i = 0 ; i < pages.length; i++){
            try {
				results.put(new Page(pages[i],new URL("http://www."+pages[i]+".com")),allFlag);
			} catch (MalformedURLException e) {}
        }
        return results;
    }
}
