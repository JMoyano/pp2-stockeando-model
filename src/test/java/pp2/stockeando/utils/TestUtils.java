package pp2.stockeando.utils;

import pp2.stockeando.model.Page;
import pp2.stockeando.model.SearchRequest;
import pp2.stockeando.validators.SearchValidator;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class TestUtils {

    public static void assertTrue(Object expected, Object actual){
        try {assert (expected.equals(actual));}
        catch (AssertionError e){
            throw new AssertionError ("expected: " + expected.toString() + ", actual: " + actual.toString());
        }
    }

    public static void assertSearchRequest(boolean expected,SearchRequest searchRequest) {
        assertTrue(expected,SearchValidator.isPagesToSearchValid(searchRequest.getPagesToSearch()));
        assertTrue(expected,SearchValidator.isProductNameValid(searchRequest.getProductName()));
        String expectedToString = "Producto a buscar: " + searchRequest.getProductName()
                    + "\nP\u00e1ginas donde buscar: " + searchRequest.getPagesToSearch().toString();
        assertTrue(expectedToString,searchRequest.toString());
    }

    public static SearchRequest buildSearchRequest(String productName){
        List<Page> pageList = new ArrayList<>();
        try {
			pageList.add(new Page("page1",new URL("http://www.page1.com")));
			pageList.add(new Page("page2",new URL("http://www.page2.com")));
		} catch (MalformedURLException e) {}
       
        SearchRequest searchRequest = new SearchRequest(pageList,productName);
        return searchRequest;
    }
}
