package pp2.stockeando.utils;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class FontStyleTest {

	@Test
	public void testFontStyle() {
		FontStyle font = new FontStyle("Tahoma",24);
		font.setName("Arial");
		font.setSize(30);
		assertEquals("Arial",font.getName());
		assertEquals(30,font.getSize());
	}
	
}
