package pp2.stockeando.notifier;

import org.junit.Test;
import pp2.stockeando.model.UserContact;
import pp2.stockeando.utils.TestUtils;

public class NotifierTest {

    private Notifier notifier = new MockNotifier();

    @Test
    public void given_MockNotifier_sendMessage(){
        boolean sent = notifier.send("mensaje",new UserContact("drkmai@gmail.com"));
        TestUtils.assertTrue(true,sent);
    }

    @Test
    public void given_MockNotifier_cantSendMessage(){
        boolean sent = notifier.send("mensaje",new UserContact(null));
        TestUtils.assertTrue(false,sent);
    }
}
