package pp2.stockeando.notifier;

import pp2.stockeando.model.UserContact;
import pp2.stockeando.validators.Validator;

public class MockNotifier implements Notifier {

    @Override
    public String getNameNotifier() {
        return "Mock";
    }

    @Override
    public boolean send(String message, UserContact receiver) {

        if (Validator.isStringValid(message) && isReceiverInfoValid(receiver)){
            if (receiver.getContactDetail().equals("MOCKFALSE")){
                return false;
            }
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public boolean isReceiverInfoValid(UserContact receiver) {
        return receiver.getContactDetail() != null;
    }

    @Override
    public int compareTo(Notifier o) {
        return this.getNameNotifier().compareTo(o.getNameNotifier());
    }
}
