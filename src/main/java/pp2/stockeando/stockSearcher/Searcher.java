package pp2.stockeando.stockSearcher;

import java.util.Map;
import java.util.TreeMap;

import pp2.stockeando.model.Page;
import pp2.stockeando.model.SearchRequest;
import pp2.stockeando.model.UserContact;
import pp2.stockeando.notifier.Notifier;
import pp2.stockeando.utils.MessageConstructor;

public class Searcher {

	private StockSearcher stockSearcher;
	
	public Searcher(StockSearcher implementation) {
		stockSearcher = implementation;
	}
	
	public SearchStockResult organizeSearchStock(SearchRequest searchRequest) {
		Map<Page,Boolean> resultsSearch = new TreeMap<>(); 
		for (Page p : searchRequest.getPagesToSearch()) {
			if(stockSearcher.search(searchRequest.getProductName(), p.getUrl())) {
				resultsSearch.put(p,true);
			}
		}
		return new SearchStockResult(resultsSearch);
	}
}
