package pp2.stockeando.stockSearcher;

import java.net.URL;

public interface StockSearcher {

    boolean search(String nameProduct,URL url);

}
