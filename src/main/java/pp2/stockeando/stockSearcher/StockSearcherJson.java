package pp2.stockeando.stockSearcher;

import java.io.File;
import java.io.FileReader;
import java.net.URL;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import pp2.stockeando.utils.PropertiesFile;

public class StockSearcherJson implements StockSearcher{

	private String pathFileJson;
	private String nameFile;
	
	public StockSearcherJson() {
		pathFileJson = System.getenv("APPDATA")+PropertiesFile.getProperty("pathFileStockInPagesJson");
		nameFile = PropertiesFile.getProperty("nameFileStockInPagesJson");
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public boolean search(String nameProduct, URL url) {
		Object ob = null;
		try {
			File carpetaJsons = new File(pathFileJson);
			for(File f : carpetaJsons.listFiles()) {  
                if(!f.getName().equals(nameFile)){
                	continue; 
                }
                else {
                	ob = new JSONParser().parse(new FileReader(f));
                }
			}
			JSONArray array = (JSONArray) ob;
			Iterator i = array.iterator();

			while (i.hasNext()) {
				JSONObject jsonObj = (JSONObject) i.next();
				URL urlPage = new URL((String)jsonObj.get("url"));
				if(!urlPage.equals(url)) {
					continue;
				}else {
					JSONArray arrayProducts = (JSONArray) jsonObj.get("productsInStock");
					Iterator iProduct = arrayProducts.iterator();
					while (iProduct.hasNext()) {
						JSONObject jsonObjProduct = (JSONObject) iProduct.next();
						String productName = (String)jsonObjProduct.get("productName");
						if(productName.equals(nameProduct)) {
							return true;
						}
						
					}
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}  
		return false;
	}
	
	public void setPath(String path) {
		pathFileJson = path;
	}

}
