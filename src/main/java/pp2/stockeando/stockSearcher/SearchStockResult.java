package pp2.stockeando.stockSearcher;

import pp2.stockeando.model.Page;

import java.util.Map;

public class SearchStockResult {

    private Map<Page,Boolean> results;
    private Boolean isInAnyPage = null;

    public SearchStockResult (Map<Page,Boolean>results){
        this.results = results;
    }

    public Map<Page, Boolean> getResults() {
        return results;
    }

    public Boolean foundInAnyPage(){
        if (isInAnyPage == null){
            isInAnyPage = false;
            for (Map.Entry<Page,Boolean> entry:results.entrySet()) {
                isInAnyPage = isInAnyPage || entry.getValue();
            }
        }
        return isInAnyPage;
    }
}
