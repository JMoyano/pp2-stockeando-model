package pp2.stockeando.validators;

import pp2.stockeando.model.Page;

import java.util.List;

public class SearchValidator extends Validator{

    public static boolean isPagesToSearchValid(List<Page> pagesToSearch) {
       if (pagesToSearch != null){
           if (pagesToSearch.size() > 0){
               return true;
           }
       }
       return false;
    }

    public static boolean isProductNameValid(String productName){
        return isStringValid(productName);
    }

}
