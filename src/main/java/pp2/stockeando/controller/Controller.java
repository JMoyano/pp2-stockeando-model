package pp2.stockeando.controller;

import java.awt.event.ActionListener;

public interface Controller extends ActionListener {

	public void updateView();
}
