package pp2.stockeando.services;

import pp2.stockeando.model.Page;

import java.util.List;

public interface PageService {
	List<Page> getAllPages();
	List<String> getAllNamePages();
	List<Page> getListPagesWithName(List<String> pagesToSearch);
}
