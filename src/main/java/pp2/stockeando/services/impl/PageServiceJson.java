package pp2.stockeando.services.impl;

import java.io.File;
import java.io.FileReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import pp2.stockeando.model.Page;
import pp2.stockeando.services.PageService;
import pp2.stockeando.utils.PropertiesFile;

public class PageServiceJson implements PageService {
	
	private List<Page> listPages;
	private String nameFileJson;
	
	public PageServiceJson(String path) {
		listPages = new ArrayList<Page>();
		nameFileJson = PropertiesFile.getProperty("nameFilePagesListJson");
		listPages.addAll(getPagesToJsonFile(path));
		
	}

	@Override
	public List<Page> getAllPages() {
		return listPages;
	}

	
	@SuppressWarnings("rawtypes")
	private List<Page> getPagesToJsonFile(String path) {
		List<Page> aux = new ArrayList<Page>();
		Object ob = null;
		try {
			File carpetaJsons = new File(path);
			for(File f : carpetaJsons.listFiles()) {  
                if(!f.getName().equals(nameFileJson)){
                	continue; 
                }
                else {
                	ob = new JSONParser().parse(new FileReader(f));
                }
			}
			JSONArray array = (JSONArray) ob;
			Iterator i = array.iterator();

			while (i.hasNext()) {
				JSONObject jsonObj = (JSONObject) i.next();
				String pageName = (String)jsonObj.get("pageName");
				URL url = new URL((String)jsonObj.get("url"));
				aux.add(new Page(pageName,url));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}  
	     return aux;
	}

	@Override
	public List<String> getAllNamePages() {
		List<String> aux = new ArrayList<String>();
		for (Page page : listPages) {
			aux.add(page.getPageName());
		}
		return aux;
	}

	@Override
	public List<Page> getListPagesWithName(List<String> pagesName) {
		List<Page> aux = new ArrayList<Page>();
		for (String name : pagesName) {
			for (Page p : this.listPages) {
				if(p.getPageName().equals(name)) {
					aux.add(p);
				}
			}
		}
		return aux;
	}
}
