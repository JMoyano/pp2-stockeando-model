package pp2.stockeando.model;

import pp2.stockeando.validators.SearchValidator;
import java.util.List;

public class SearchRequest {

    private List<Page> pagesToSearch;
    private String productName;

    public SearchRequest(List<Page> pagesToSearch, String productName) {
        setProductName(productName);
        setPagesToSearch(pagesToSearch);
    }

    @Override
    public String toString(){
        return "Producto a buscar: " + this.productName
        + "\nP\u00e1ginas donde buscar: " + this.pagesToSearch.toString();
   
    }

    private void setPagesToSearch(List<Page>pagesToSearch){
        if (!SearchValidator.isPagesToSearchValid(pagesToSearch))
            throw new IllegalArgumentException("Ten\u00E9s que elegir al menos una p\u00e1gina donde buscar.");
        this.pagesToSearch = pagesToSearch;
    }

    private void setProductName(String productName){
        if (!SearchValidator.isProductNameValid(productName))
            throw new IllegalArgumentException("El nombre no es v\u00e1lido.");
        this.productName = productName;
    }

    public List<Page> getPagesToSearch() {
        return pagesToSearch;
    }

    public String getProductName() {
        return productName;
    }

}
