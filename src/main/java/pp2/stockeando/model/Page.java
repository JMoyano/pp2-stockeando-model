package pp2.stockeando.model;

import pp2.stockeando.validators.PageValidator;

import java.net.URL;
import java.util.Objects;


public class Page implements Comparable<Page>{

    private String pageName;
    private URL url;


    public Page(String pageName, URL url) {
       setPageName(pageName);
       setUrl(url);
    }

    public String getPageName() {
        return pageName;
    }

    public URL getUrl() {
        return url;
    }

    private void setUrl(URL url){
        this.url = url;
    }

    private void setPageName(String pageName){
        if (!PageValidator.isPageNameValid(pageName)){
            throw new IllegalArgumentException("El nombre de la p\u00e1gina no es v\u00e1lido.");
        }
        this.pageName = pageName;
    }

    @Override
    public String toString(){
        return getPageName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Page page = (Page) o;
        return Objects.equals(pageName, page.pageName) && Objects.equals(url, page.url);
    }

    @Override
    public int compareTo(Page other) {
        return pageName.compareTo(other.getPageName());
    }
}
