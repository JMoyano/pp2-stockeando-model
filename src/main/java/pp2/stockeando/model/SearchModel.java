package pp2.stockeando.model;

import java.util.List;
import java.util.Map;

import pp2.stockeando.controller.Controller;
import pp2.stockeando.notifier.Notifier;
import pp2.stockeando.notifier.NotifierData;
import pp2.stockeando.services.PageService;
import pp2.stockeando.services.impl.PageServiceJson;
import pp2.stockeando.stockSearcher.Searcher;
import pp2.stockeando.stockSearcher.SearchStockResult;
import pp2.stockeando.stockSearcher.StockSearcher;
import pp2.stockeando.stockSearcher.StockSearcherJson;
import pp2.stockeando.utils.MessageConstructor;
import pp2.stockeando.utils.PropertiesFile;

public class SearchModel {
	public String actionStr;
	private NotifierData dataNotifier;
	private PageService pageService;
	private Controller subscribeController;
	private Searcher searcher;
	
	public SearchModel() {
		pageService = new PageServiceJson(System.getenv("APPDATA")+PropertiesFile.getProperty("pathFilePagesListJson"));
		dataNotifier = new NotifierData(System.getenv("APPDATA")+PropertiesFile.getProperty("pathNotifier"));
		searcher = new Searcher(new StockSearcherJson());
	}
	
    public void searchProduct(List<String> pagesToSearch, String productName, Map<String,UserContact> communicationOptions) {
    	List<Page> pageList = pageService.getListPagesWithName(pagesToSearch);
    	SearchStockResult searchResult = searcher.organizeSearchStock(new SearchRequest(pageList,productName));
		String message = MessageConstructor.constructMessage(searchResult, productName);

		boolean sendResult = false;
		String channelThatWorked = "";
		for (Map.Entry<String,UserContact> entry : communicationOptions.entrySet()) {
			Notifier notifierSelected = dataNotifier.getNotifierByName(entry.getKey());
			sendResult = notifierSelected.send(message,entry.getValue());
			if (sendResult) {
				channelThatWorked = notifierSelected.getNameNotifier();
				break;
			}
		}
		if(sendResult) {
			actionStr = "Notificacion enviada correctamente por "+channelThatWorked;
		}else {
			actionStr = "Problema al enviar la notificacion";
		}
		subscribeController.updateView();
    }
    
    public String getAction() {
   		return actionStr;
    }
    public void setAction(String action) {
   		this.actionStr = action;
    }
	public List<String> getPages(){
		return pageService.getAllNamePages();
	}
	
	public List<String> getChannelsNotifier(){
		return dataNotifier.getNamesNotifiersAvailable();
	}
	
	public void setSubscribeController(Controller subscribeController) {
		this.subscribeController=subscribeController;
	}

	public boolean isReceiverInfoValid(String notifierName, UserContact userContact) {
		return dataNotifier.validInfoReceiverByNameNotifier(notifierName, userContact);
	}
	public void setPathNotifiersData(String path) {
		dataNotifier = new NotifierData(path);
	}
	public void setPathPageService(String path) {
		pageService = new PageServiceJson(path);
	}

	public void setSearcher(StockSearcher stockSearcher) {
		searcher = new Searcher(stockSearcher);
	}
}
