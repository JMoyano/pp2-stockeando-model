package pp2.stockeando.model;

public class UserContact {

    protected String contactDetail;

    public UserContact(String contactDetail){
        this.contactDetail = contactDetail;
    }

    public String getContactDetail() {
        return contactDetail;
    }


    @Override
    public String toString() {
        return contactDetail;
    }


}
