package pp2.stockeando.notifier;

import pp2.stockeando.model.UserContact;

public interface Notifier extends Comparable<Notifier>{
	
	String getNameNotifier();
	boolean send(String message, UserContact receiver);

	boolean isReceiverInfoValid(UserContact receiver);
}
