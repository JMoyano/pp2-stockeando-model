package pp2.stockeando.notifier;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import pp2.stockeando.model.UserContact;

public class NotifierData {

	private Set<Notifier> notifiers;
	
	public NotifierData(String pathToFindNotifiers) {
		this.notifiers = NotifierFinder.findNotifiers(pathToFindNotifiers);
	}
	
	public List<String> getNamesNotifiersAvailable(){
		List<String> namesNotifiers = new ArrayList<String>();
		if(!this.notifiers.isEmpty()) {
			for (Notifier notifier:notifiers) {
		           namesNotifiers.add(notifier.getNameNotifier());
		    }
		}
		return namesNotifiers;
	}
	
	public boolean validInfoReceiverByNameNotifier(String nameNotifier, UserContact contactDetail) {
		for (Notifier notifier : notifiers) {
			if(notifier.getNameNotifier().equals(nameNotifier)) {
				return notifier.isReceiverInfoValid(contactDetail);
			}
		}
		return false;
		
	}
	
	public Notifier getNotifierByName(String nameNotifier) {
		for (Notifier notifier : notifiers) {
			if(notifier.getNameNotifier().equals(nameNotifier)) {
				return notifier;
			}
		}
		return null;
	}
	public Set<Notifier> getNotifiers(){
		return notifiers;
	}
}
