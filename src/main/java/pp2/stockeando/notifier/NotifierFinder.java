package pp2.stockeando.notifier;


import pp2.stockeando.utils.JarProcessor;

import java.util.Set;

public class NotifierFinder {

	private NotifierFinder(){}

	public static Set<Notifier> findNotifiers(String path){
		return JarProcessor.findByClass(Notifier.class,path);
	}
}
