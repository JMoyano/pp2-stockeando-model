package pp2.stockeando.utils;

import java.io.InputStream;
import java.util.Properties;

public class PropertiesFile {

	 public static String getProperty(String propertyName) {
		 String resourceName = "config.properties"; 
		 ClassLoader loader = Thread.currentThread().getContextClassLoader();
		 Properties props = new Properties();
		 InputStream resourceStream = loader.getResourceAsStream(resourceName);
		 try {
			 props.load(resourceStream); 
			 return  props.getProperty(propertyName);
		 }catch (Exception e) {
			 e.printStackTrace();
		 }
		 return "";
	 }
	
}
