package pp2.stockeando.utils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class JarProcessor {
    private JarProcessor() {}

    public static <T> Set<T> findByClass(Class<? extends T> classToFind,String path){
        File carpetaImplementaciones = new File(path);
        Set<T> set = new TreeSet<>();
        try {
	        for(File f : carpetaImplementaciones.listFiles()) {  
	                set.addAll(createInstances(findClassesInFile(f, classToFind)));
	        }
        } catch (Exception e) {
        	return set;
        }
        return set;
    }

    public static <T> List<T> createInstances(Collection<Class<? extends T>> classes, Object... constructorParameters)  {
        try {
            List<T> ret = new ArrayList<>();
            Class<?>[] parameterClasses = new Class<?>[constructorParameters.length];
            for(int i = 0; i < constructorParameters.length; i++)
                parameterClasses[i] = constructorParameters[i].getClass();

            for(Class<? extends T> c : classes)
                ret.add(c.getDeclaredConstructor(parameterClasses).newInstance(constructorParameters));

            return ret;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
		
    }

    public static <T> List<Class<? extends T>> findClassesInFile(File jarFile, Class<T> c)
    {
        try {
            URL fileURL = jarFile.toURI().toURL();
            String jarURL = "jar:" + fileURL + "!/";
            URL[] urls = { new URL(jarURL) };
            try(URLClassLoader ucl = new URLClassLoader(urls)) {
                return getFromList(ucl, findClassNames(jarFile), c);
            }
        } catch(Exception e) {
        	  e.printStackTrace();
        	  return null;
        }
    }

    @SuppressWarnings("unchecked")
    private static <T> List<Class<? extends T>> getFromList(URLClassLoader ucl, List<String> classNames, Class<T> c) throws ClassNotFoundException
    {
        List<Class<? extends T>> ret = new ArrayList<>();
        for(String className : classNames)
        {
            Class<?> foundClass = Class.forName(className, true, ucl);
            if(c.isAssignableFrom(foundClass))
                ret.add((Class<? extends T>) foundClass);
        }
        return ret;
    }

    private static List<String> findClassNames(File pathToJar) throws IOException
    {
        List<String> ret = new ArrayList<>();

        JarFile jarFile = new JarFile(pathToJar);
        Enumeration<JarEntry> e = jarFile.entries();
        while (e.hasMoreElements()) {
            JarEntry je = e.nextElement();
            if(je.getName().endsWith(".class"))
                ret.add(je.getName().replace(".class", "").replace("/", "."));
        }
        jarFile.close();
        return ret;
    }
}
