package pp2.stockeando.utils;

import pp2.stockeando.model.Page;
import pp2.stockeando.stockSearcher.SearchStockResult;

import java.util.Map;

public class MessageConstructor {

    public static String constructMessage(SearchStockResult processResult, String productName){

        StringBuilder stringBuilder = new StringBuilder();

        if (processResult.foundInAnyPage()){
            stringBuilder = stringBuilder.append("Se encontro el producto " + productName + " en:\n");
            for (Map.Entry<Page,Boolean> entry:processResult.getResults().entrySet()) {
               if (entry.getValue() == true){
                   stringBuilder = stringBuilder.append(entry.getKey().getPageName() + ", " + entry.getKey().getUrl() + "\n");
               }
            }
        }
        else{
            stringBuilder = stringBuilder.append("No se encontro el producto " + productName + " en ninguna de las paginas seleccionadas.\n");
        }

        stringBuilder = stringBuilder.append("Gracias por usar Stockeando!");

        return stringBuilder.toString();
    }
}
